# Streamlit App with a Hugging Face AI Text Generative Model

This project aims to develop a website with Streamlit, integrate with an open-source Large Language Model (LLM) from Hugging Face, and deploy the model through Streamlit for universal access through a web browser.

## Goals
* Create a website using Streamlit
* Connect to an open source LLM (Hugging Face)
* Deploy model via Streamlit or other service (accessible via browser)

## Steps
### Step 1: Setup
1. Install necessary libraries
```
pip install streamlit transformers
```
2. Navigate to the project directory. Install the necessary Python libraries using pip:
```
pip install -r requirements.txt
```
### Step 2: Create the Streamlit Web Application
Create a Python script to implement the app function:
1. Load the LLM model
```
model = pipeline("text-generation", model="openai-gpt")
```
2. Define the aesthetics and the detailed implementation of Streamlit app.
### Step 3: Run locally
Navigate to the project directory:
```
streamlit run streamlit_app.py
```
### Step 4: Model Deployment
1. Create a `setup.sh` file to instruct how to run the app:
```
mkdir -p ~/.streamlit/
echo "\
[server]\n\
headless = true\n\
port = $PORT\n\
enableCORS = false\n\
\n\
" > ~/.streamlit/config.toml
```
2. Deploy using Streamlit Sharing CLI:
```
streamlit login
streamlit deploy your-email@example.com
```
3. After the successful deployment, we can get the URL of the app:
https://npgtpd6psm4d7ajwc8uc34.streamlit.app/

## Results
![llm_1](https://gitlab.com/dukeaiml/IDS721/huggingface_llm/-/wikis/uploads/79978815c1e7544f39c00a92456e3440/llm_1.png)
![llm_2](https://gitlab.com/dukeaiml/IDS721/huggingface_llm/-/wikis/uploads/68e80c7e87dc3b9dda6d53a66efc3799/llm_2.png)
